FROM debian:buster
LABEL  maintainer="didier@aeberhard.me"

ARG sftp_user=sftpuser
ENV SFTP_PASSWORD=password
ARG new_password
ENV NEW_SFTP_PASSWORD=${new_password}

# Steps done in one RUN layer:
# - Install packages
# - OpenSSH needs /var/run/sshd to run
# - Remove generic host keys, entrypoint generates unique keys
RUN apt-get update && \
    apt-get -y install openssh-server && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /var/run/sshd && \
    rm -f /etc/ssh/ssh_host_*key*

COPY files/sshd_config /etc/ssh/sshd_config
COPY files/entrypoint /
COPY ssh_keys /etc/ssh

RUN chmod -R g=u /etc/ssh && \
    chmod g=u /etc/passwd && \
    chmod -R g=u /run && \
    mkdir /sftp && \
    chgrp -R 0 /sftp && \
    chmod -R g=u /sftp

RUN useradd -u 1001 -g 0 -d /sftp -s /bin/bash ${sftp_user} && \
    echo "${sftp_user}:${SFTP_PASSWORD}" | chpasswd
#RUN echo "${sftp_user}:${SFTP_PASSWORD}:1001:0:sftpuser:/sftp:bin/bash" >> /etc/passwd
#RUN echo "${sftp_user}:${SFTP_PASSWORD}" | chpasswd

RUN chown ${sftp_user}:root /etc/ssh/ssh_host_*key.pub

EXPOSE 2022

USER ${sftp_user}
WORKDIR /sftp

ENTRYPOINT ["/entrypoint"]